package ru.renessans.jvschool.volkov.task.manager.exception.invalid.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidUserRoleException extends AbstractException {

    @NotNull
    private static final String EMPTY_USER_ROLE = "Ошибка! Параметр \"тип пользователя\" является null!\n";

    public InvalidUserRoleException() {
        super(EMPTY_USER_ROLE);
    }

}