package ru.renessans.jvschool.volkov.task.manager.exception.invalid.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidLoginException extends AbstractException {

    @NotNull
    private static final String EMPTY_LOGIN = "Ошибка! Параметр \"логин\" является пустым или null!\n";

    public InvalidLoginException() {
        super(EMPTY_LOGIN);
    }

}