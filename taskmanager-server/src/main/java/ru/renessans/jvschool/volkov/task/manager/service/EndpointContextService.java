package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AbstractEndpoint;

import java.util.List;

public final class EndpointContextService implements IEndpointContextService {

    private final IEndpointContextRepository endpointRepository;

    public EndpointContextService(
            final IEndpointContextRepository endpointRepository
    ) {
        this.endpointRepository = endpointRepository;
    }

    @NotNull
    @Override
    public List<AbstractEndpoint> createEndpoints(@NotNull final IServiceContextService serviceLocator) {
        @NotNull final List<AbstractEndpoint> endpoints = this.endpointRepository.getAllEndpoints();
        endpoints.forEach(endpoint -> endpoint.setServiceLocator(serviceLocator));
        return endpoints;
    }

}