package ru.renessans.jvschool.volkov.task.manager.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Date;

@Getter
@Setter
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public final class TimeFrame {

    @NotNull
    @Column(nullable = false, updatable = false)
    private Date creationDate = new Date(System.currentTimeMillis());

}