package ru.renessans.jvschool.volkov.task.manager.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AbstractEndpoint;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.util.EndpointUtil;

import java.util.Collection;
import java.util.List;

public final class Bootstrap {

    @NotNull
    private final IServiceContextRepository serviceContextRepository = new ServiceContextRepository();

    @NotNull
    private final IServiceContextService serviceContext = new ServiceContextService(serviceContextRepository);

    @NotNull
    private final IEndpointContextRepository endpointContextRepository = new EndpointContextRepository();

    @NotNull
    private final EndpointContextService endpointContext = new EndpointContextService(endpointContextRepository);

    public void run() {
        loadConfiguration();
        buildEntityManagerFactory();
        initialDemoData();
        publishWebServices();
    }

    private void loadConfiguration() {
        @NotNull final IPropertyService configService = this.serviceContext.getPropertyService();
        configService.load();
    }

    private void buildEntityManagerFactory() {
        @NotNull final IEntityManagerFactoryService connectionService = this.serviceContext.getEntityManagerService();
        connectionService.build();
    }

    private void initialDemoData() {
        @NotNull final IUserService userService = this.serviceContext.getUserService();
        @NotNull final Collection<User> users = userService.initialDemoData();
        @NotNull final ITaskUserService taskService = this.serviceContext.getTaskService();
        taskService.initialDemoData(users);
        @NotNull final IProjectUserService projectService = this.serviceContext.getProjectService();
        projectService.initialDemoData(users);
    }

    private void publishWebServices() {
        @NotNull final List<AbstractEndpoint> endpoints = this.endpointContext.createEndpoints(this.serviceContext);
        @Nullable final IConfigurationService configService = this.serviceContext.getConfigurationService();
        @Nullable final String host = configService.getServerHost();
        @Nullable final Integer port = configService.getServerPort();
        EndpointUtil.publish(endpoints, host, port);
    }

}