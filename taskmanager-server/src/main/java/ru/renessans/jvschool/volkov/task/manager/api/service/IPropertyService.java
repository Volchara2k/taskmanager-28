package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    void load();

    void load(
            @Nullable String name
    );

    @NotNull
    String getStringProperty(
            @Nullable String key
    );

    @NotNull
    Integer getIntegerProperty(
            @Nullable String key
    );

}