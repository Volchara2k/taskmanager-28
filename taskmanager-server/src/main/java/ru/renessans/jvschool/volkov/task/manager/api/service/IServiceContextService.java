package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceContextService {

    @NotNull
    IUserService getUserService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IAuthenticationService getAuthenticationService();

    @NotNull
    ITaskUserService getTaskService();

    @NotNull
    IProjectUserService getProjectService();

    @NotNull
    IDataInterChangeService getDataInterChangeService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IConfigurationService getConfigurationService();

    @NotNull
    IEntityManagerFactoryService getEntityManagerService();

    @NotNull
    IAdapterContextService getAdapterService();

}