package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalSetAllModelException extends AbstractException {

    @NotNull
    private static final String MODEL_SET_ALL_ILLEGAL =
            "Ошибка! Выполнение установки значений моделей в базу данных завершилось нелегальным образом!\n";

    public IllegalSetAllModelException(@NotNull final Throwable cause) {
        super(MODEL_SET_ALL_ILLEGAL, cause);
    }

}