package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IPropertyService;

@AllArgsConstructor
public final class ConfigurationService implements IConfigurationService {

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SESSION_CYCLE_KEY = "session.cycle";

    @NotNull
    private static final String SESSION_SALT_KEY = "session.salt";

    @NotNull
    private static final String PATHNAME_BIN = "file.name.bin";

    @NotNull
    private static final String PATHNAME_BASE64 = "file.name.base64";

    @NotNull
    private static final String PATHNAME_JSON = "file.name.json";

    @NotNull
    private static final String PATHNAME_XML = "file.name.xml";

    @NotNull
    private static final String PATHNAME_YAML = "file.name.yaml";

    @NotNull
    private static final String JDBC_DRIVER = "jdbc.driver";

    @NotNull
    private static final String JDBC_URL = "jdbc.url";

    @NotNull
    private static final String DATABASE_LOGIN = "db.login";

    @NotNull
    private static final String DATABASE_PASSWORD = "db.password";

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    @Override
    public String getServerHost() {
        return this.propertyService.getStringProperty(SERVER_HOST_KEY);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return this.propertyService.getIntegerProperty(SERVER_PORT_KEY);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return this.propertyService.getStringProperty(SESSION_SALT_KEY);
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        return this.propertyService.getIntegerProperty(SESSION_CYCLE_KEY);
    }

    @NotNull
    @Override
    public String getBinPathname() {
        return this.propertyService.getStringProperty(PATHNAME_BIN);
    }

    @NotNull
    @Override
    public String getBase64Pathname() {
        return this.propertyService.getStringProperty(PATHNAME_BASE64);
    }

    @NotNull
    @Override
    public String getJsonPathname() {
        return this.propertyService.getStringProperty(PATHNAME_JSON);
    }

    @NotNull
    @Override
    public String getXmlPathname() {
        return this.propertyService.getStringProperty(PATHNAME_XML);
    }

    @NotNull
    @Override
    public String getYamlPathname() {
        return this.propertyService.getStringProperty(PATHNAME_YAML);
    }

    @NotNull
    @Override
    public String getDriverJDBC() {
        return this.propertyService.getStringProperty(JDBC_DRIVER);
    }

    @NotNull
    @Override
    public String getUrlJDBC() {
        return this.propertyService.getStringProperty(JDBC_URL);
    }

    @NotNull
    @Override
    public String getDatabaseLogin() {
        return this.propertyService.getStringProperty(DATABASE_LOGIN);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return this.propertyService.getStringProperty(DATABASE_PASSWORD);
    }

}