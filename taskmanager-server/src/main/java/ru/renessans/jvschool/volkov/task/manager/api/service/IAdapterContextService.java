package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.*;

public interface IAdapterContextService {

    @NotNull
    IProjectAdapterService getProjectAdapter();

    @NotNull
    ISessionAdapterService getSessionAdapter();

    @NotNull
    ITaskAdapterService getTaskAdapter();

    @NotNull
    IUserLimitedAdapterService getUserLimitedAdapter();

    @NotNull
    IUserUnlimitedAdapterService getUserUnlimitedAdapter();

}