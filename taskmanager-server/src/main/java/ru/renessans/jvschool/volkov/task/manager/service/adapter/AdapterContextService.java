package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IAdapterContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAdapterContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.*;

@AllArgsConstructor
public final class AdapterContextService implements IAdapterContextService {

    private final IAdapterContextRepository adapterRepository;

    @NotNull
    public IProjectAdapterService getProjectAdapter() {
        return this.adapterRepository.getProjectAdapter();
    }

    @NotNull
    public ISessionAdapterService getSessionAdapter() {
        return this.adapterRepository.getSessionAdapter();
    }

    @NotNull
    public ITaskAdapterService getTaskAdapter() {
        return this.adapterRepository.getTaskAdapter();
    }

    @NotNull
    public IUserLimitedAdapterService getUserLimitedAdapter() {
        return this.adapterRepository.getUserLimitedAdapter();
    }

    @NotNull
    @Override
    public IUserUnlimitedAdapterService getUserUnlimitedAdapter() {
        return this.adapterRepository.getUserUnlimitedAdapter();
    }

}