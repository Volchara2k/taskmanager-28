package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidPropertyFetchException extends AbstractException {

    @NotNull
    private static final String EMPTY_PROPERTY_FETCH = "" +
            "Ошибка! Выполнение извлечения свойства из файла завершилось нелегальным образом!\n";

    public InvalidPropertyFetchException() {
        super(EMPTY_PROPERTY_FETCH);
    }

}