package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IRepository;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    Collection<User> getAllRecords();

    @Nullable
    User getById(@NotNull String id);

    @Nullable
    User getByLogin(@NotNull String login);

    @NotNull
    User deleteById(@NotNull String id);

    @Nullable
    User deleteByLogin(@NotNull String login);

}