package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IRepository;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import java.util.Collection;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    Session getById(@NotNull final String id);

    boolean containsUserId(@NotNull String userId);

    @Nullable
    Session getSessionByUserId(@NotNull String userId);

    @NotNull
    Collection<Session> getAllRecords();

}