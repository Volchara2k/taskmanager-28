package ru.renessans.jvschool.volkov.task.manager.exception.invalid.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidUserException extends AbstractException {

    @NotNull
    private static final String EMPTY_USER =
            "Ошибка! Параметр \"пользователь\" является null!\n";

    public InvalidUserException() {
        super(EMPTY_USER);
    }

}