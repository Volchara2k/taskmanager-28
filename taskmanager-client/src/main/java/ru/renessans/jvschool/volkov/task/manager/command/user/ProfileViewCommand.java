package ru.renessans.jvschool.volkov.task.manager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class ProfileViewCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_VIEW_PROFILE = "view-profile";

    @NotNull
    private static final String DESC_VIEW_PROFILE = "обновить пароль пользователя";

    @NotNull
    private static final String NOTIFY_VIEW_PROFILE =
            "Происходит попытка инициализации отображения информации о текущем пользователе \n" +
                    "Информация о текущем профиле пользователя: ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_VIEW_PROFILE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_VIEW_PROFILE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_VIEW_PROFILE);

        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final UserEndpoint userEndpoint = endpointLocator.getUserEndpoint();

        @Nullable final UserLimitedDTO user = userEndpoint.getUser(current);
        ViewUtil.print(user);
    }

}