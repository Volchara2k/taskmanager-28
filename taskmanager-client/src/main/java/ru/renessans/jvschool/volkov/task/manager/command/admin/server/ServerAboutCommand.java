package ru.renessans.jvschool.volkov.task.manager.command.admin.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class ServerAboutCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_SERVER_ABOUT = "server-about";

    @NotNull
    private static final String DESC_SERVER_ABOUT = "получить информацию о сервере (администратор)";

    @NotNull
    private static final String NOTIFY_SERVER_ABOUT =
            "Происходит попытка инициализации просмотра информации о сервере системы...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_SERVER_ABOUT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_SERVER_ABOUT;
    }

    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final AdminEndpoint adminEndpoint = endpointLocator.getAdminEndpoint();

        ViewUtil.print(NOTIFY_SERVER_ABOUT);
        @NotNull final String serverAbout = adminEndpoint.serverData(current);
        ViewUtil.print(serverAbout);
    }

}