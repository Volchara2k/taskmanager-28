package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.EmptySessionException;

import java.util.Objects;

public final class CurrentSessionRepository implements ICurrentSessionRepository {

    @Nullable
    private SessionDTO session;

    @NotNull
    @Override
    public SessionDTO put(@NotNull final SessionDTO session) {
        this.session = session;
        return session;
    }

    @Nullable
    @Override
    public SessionDTO get() {
        return this.session;
    }

    @NotNull
    @SneakyThrows
    @Override
    public SessionDTO delete() {
        @Nullable final SessionDTO deleted = get();
        if (Objects.isNull(deleted)) throw new EmptySessionException();
        this.session = null;
        return deleted;
    }

}