package ru.renessans.jvschool.volkov.task.manager.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IApplicationContextService;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.exception.unknown.UnknownCommandException;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.service.ApplicationContextService;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.util.ScannerUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Objects;

public final class Bootstrap {

    @NotNull
    private final IServiceContextRepository serviceContextRepository = new ServiceContextRepository();

    @NotNull
    private final IServiceContextService serviceContext = new ServiceContextService(serviceContextRepository);

    @NotNull
    private final IEndpointContextRepository endpointContextRepository = new EndpointContextRepository();

    @NotNull
    private final IEndpointContextService endpointContext = new EndpointContextService(endpointContextRepository);

    @NotNull
    private final ICommandService commandService = serviceContext.getCommandService();

    @NotNull
    private final IApplicationContextService applicationContext = new ApplicationContextService(
            endpointContext, serviceContext
    );

    {
        this.commandService.createCommands(this.applicationContext);
    }

    public void run(@Nullable final String... arguments) {
        final boolean isEmptyArgs = ValidRuleUtil.isNullOrEmpty(arguments);
        if (isEmptyArgs) terminalCommandExecuteLoop();
        else argumentExecute(Objects.requireNonNull(arguments[0]));
    }

    @SuppressWarnings("InfiniteLoopStatement")
    private void terminalCommandExecuteLoop() {
        @NotNull String commandLine;
        while (true) {
            try {
                commandLine = ScannerUtil.getLine();
                @Nullable final AbstractCommand command = this.commandService.getTerminalCommand(commandLine);
                if (Objects.isNull(command)) throw new UnknownCommandException(commandLine);
                command.execute();
            } catch (@NotNull final Exception exception) {
                System.err.print(exception.getMessage() + "\n");
            }
        }
    }

    private void argumentExecute(@NotNull final String argument) {
        try {
            @Nullable final AbstractCommand command = this.commandService.getArgumentCommand(argument);
            if (Objects.isNull(command)) throw new UnknownCommandException(argument);
            command.execute();
        } catch (@NotNull final Exception exception) {
            System.err.print(exception.getMessage() + "\n");
        }
    }

}