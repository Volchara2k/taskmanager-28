package ru.renessans.jvschool.volkov.task.manager.command;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IApplicationContextService;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

@Setter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractCommand {

    @NotNull
    protected IApplicationContextService applicationContext;

    @NotNull
    public abstract String getCommand();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        result.append("Терминальная команда: ").append(getCommand());
        if (ValidRuleUtil.isNotNullOrEmpty(getArgument()))
            result.append(", программный аргумент: ").append(getArgument());
        result.append("\n\t - ").append(getDescription());
        return result.toString();
    }

}