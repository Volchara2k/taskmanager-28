package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.EmptySessionException;

import java.util.Objects;

@AllArgsConstructor
public final class CurrentSessionService implements ICurrentSessionService {

    @NotNull
    private final ICurrentSessionRepository sessionRepository;

    @Nullable
    @SneakyThrows
    @Override
    public SessionDTO getSession() {
        return this.sessionRepository.get();
    }

    @NotNull
    @SneakyThrows
    @Override
    public SessionDTO subscribe(
            @Nullable final SessionDTO session
    ) {
        if (Objects.isNull(session)) throw new EmptySessionException();
        return this.sessionRepository.put(session);
    }

    @NotNull
    @SneakyThrows
    @Override
    public SessionDTO unsubscribe() {
        return this.sessionRepository.delete();
    }

}