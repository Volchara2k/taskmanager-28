package ru.renessans.jvschool.volkov.task.manager.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class UserDeleteCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_USER_DELETE = "user-delete";

    @NotNull
    private static final String DESC_USER_DELETE = "удалить пользователя (администратор)";

    @NotNull
    private static final String NOTIFY_USER_DELETE =
            "Происходит попытка инициализации удаления пользователя системы. \n" +
                    "Для удаления пользователя введите его логин. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_USER_DELETE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_USER_DELETE;
    }

    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final AdminEndpoint adminEndpoint = endpointLocator.getAdminEndpoint();

        ViewUtil.print(NOTIFY_USER_DELETE);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final UserLimitedDTO user = adminEndpoint.deleteUserByLogin(current, login);
        ViewUtil.print(user);
    }

}