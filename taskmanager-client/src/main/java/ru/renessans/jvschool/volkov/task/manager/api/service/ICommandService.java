package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IApplicationContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    @NotNull
    Collection<AbstractCommand> createCommands(
            @Nullable IApplicationContextService applicationContext
    );

    @Nullable
    Collection<AbstractCommand> getAllCommands();

    @Nullable
    Collection<AbstractCommand> getAllTerminalCommands();

    @Nullable
    Collection<AbstractCommand> getAllArgumentCommands();

    @Nullable
    AbstractCommand getTerminalCommand(
            @Nullable String commandLine
    );

    @Nullable
    AbstractCommand getArgumentCommand(
            @Nullable String commandLine
    );

}