package ru.renessans.jvschool.volkov.task.manager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class PasswordUpdateCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_UPDATE_PASSWORD = "update-password";

    @NotNull
    private static final String ARG_UPDATE_PASSWORD = "обновить пароль пользователя";

    @NotNull
    private static final String NOTIFY_UPDATE_PASSWORD =
            "Происходит попытка инициализации смены пароля. \n" +
                    "Для смены пароля введите новый пароль. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_UPDATE_PASSWORD;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return ARG_UPDATE_PASSWORD;
    }

    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();

        ViewUtil.print(NOTIFY_UPDATE_PASSWORD);
        @Nullable final SessionDTO current = currentSessionService.getSession();
        @NotNull final String password = ViewUtil.getLine();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final UserEndpoint userEndpoint = endpointLocator.getUserEndpoint();
        @Nullable final UserLimitedDTO user = userEndpoint.updatePassword(current, password);
        ViewUtil.print(user);
    }

}