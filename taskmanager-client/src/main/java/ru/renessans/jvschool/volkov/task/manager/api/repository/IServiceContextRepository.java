package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;

public interface IServiceContextRepository {

    @NotNull
    ICurrentSessionService getCurrentSession();

    @NotNull
    ICommandService getCommandService();

}