package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.IApplicationContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;

@AllArgsConstructor
public final class ApplicationContextService implements IApplicationContextService {

    @NotNull
    private final IEndpointContextService endpointLocatorService;

    @NotNull
    private final IServiceContextService serviceLocatorService;

    @NotNull
    @Override
    public IEndpointContextService getEndpointContext() {
        return this.endpointLocatorService;
    }

    @NotNull
    @Override
    public IServiceContextService getServiceContext() {
        return this.serviceLocatorService;
    }

}